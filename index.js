const express = require('express');
const bodyParser = require('body-parser');
const { Expo } = require('expo-server-sdk');

const app = express ();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

let expo = new Expo();

let expoPushToken;
// let messages = [];

app.post('/token', (req, res) => {
    if(!req.body.token.value) {
        res.status(400).json({message: 'token is not provided'});
    }
    console.log(req.body.message.value);
    expoPushToken = req.body.token.value;
    if(!Expo.isExpoPushToken(expoPushToken)) {
        console.error(`Push token ${expoPushToken} is not a valid Expo push token`);
        res.status(400).json({message: 'token is invalid'});
    }
    console.log(expoPushToken);
    res.status(200).json({message: 'success'});
});

app.post('/message', async(req, res) => {
    if(!req.body.msgTitle || !req.body.msgBody || !expoPushToken) {
        res.status(400).json({message: 'body and title are not provided'});
        return;
    }

    let messages = [];
    messages.push ({
        to: expoPushToken,
        body: req.body.msgBody,
        title: req.body.msgTitle,
        channelId: 'orders'
    });
    // let chunks = expo.chunkPushNotifications(messages);

    try {
        let ticketChunk = await expo.sendPushNotificationsAsync(messages);
        console.log(ticketChunk);
        res.status(200).json({message: 'success'});
    } catch(err) {
        console.log(err);
        res.status(400).json({message: 'error occured'});
    }  

    /*
    for (let chunk of chunks) {
        try {
            let ticketChunk = await expo.sendPushNotificationsAsync(chunk);
            console.log(ticketChunk);
            res.status(200).json({message: 'success'});
        } catch(err) {
            console.log(err);
            res.status(400).json({message: 'error occured'});
        }  
    }
    */
});

process.on ("SIGINT", () => {
    console.log('\nreceive sigint event');
    process.exit ();
})

const server = app.listen (3000, (err) => {
    if (err)
        console.log('error occured!');
    else
        console.log('server is running at port 3000');
})
